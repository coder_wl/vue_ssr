import {App, ssrContextKey} from 'vue'
import {createApp as _createApp} from './main'
import {renderToString} from '@vue/server-renderer'
import {RouteLocationRaw} from 'vue-router'

export function createApp () {
    return _createApp(true);
};

export async function render(app: App, url: RouteLocationRaw, manifest: any) {

    // set the router to the desired URL before rendering
    await app.config.globalProperties.$router.push(url)
    await app.config.globalProperties.$router.isReady()

    // passing SSR context object which will be available via useSSRContext()
    // @vitejs/plugin-vue injects code into a component's setup() that registers
    // itself on ctx.modules. After the render, ctx.modules would contain all the
    // components that have been instantiated during this render call.
    const ctx: any = {}
    const html = await renderToString(app, ctx);

    // 清除 ssrContext provide，防止在使用对象池时，警告 provide 已存在
    // @ts-ignore
    if (app._context.provides[ssrContextKey]) {
        // @ts-ignore
        delete app._context.provides[ssrContextKey];
    }


    const _myMeta = app.config.globalProperties.$myMeta.renderToString();


    // the SSR manifest generated by Vite contains module -> chunk/asset mapping
    // which we can then use to determine what files need to be preloaded for this
    // request.
    const preloadLinks = renderPreloadLinks(ctx.modules, manifest)
    return [html, preloadLinks, _myMeta]
}

function renderPreloadLinks(modules: any[], manifest: commonObject<any>) {
    let links = ''
    const seen = new Set()
    modules.forEach((id) => {
        const files = manifest[id]
        if (files) {
            files.forEach((file: string) => {
                if (!seen.has(file)) {
                    seen.add(file)
                    links += renderPreloadLink(file)
                }
            })
        }
    })
    return links
}

function renderPreloadLink(file: string) {
    if (file.endsWith('.css')) {
        return `<link ref="preload" rel="stylesheet" href="${file}">`
    } else if (file.endsWith('.woff')) {
        return ` <link rel="preload" href="${file}" as="font" type="font/woff" crossorigin>`
    } else if (file.endsWith('.woff2')) {
        return ` <link rel="preload" href="${file}" as="font" type="font/woff2" crossorigin>`
    } else {
        // TODO
        return ''
    }
}
