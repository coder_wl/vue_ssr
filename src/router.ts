import {createMemoryHistory, createRouter as _createRouter, createWebHistory} from 'vue-router'

export function createRouter() {
    const routes = [
        {
            path: '/',
            component: () => import('./pages/Index.vue'),
        },
        {
            path: '/page',
            component: () => import('./pages/Page.vue'),
        }
    ];

    return _createRouter({
        history: import.meta.env.SSR ? createMemoryHistory() : createWebHistory(),
        routes,
        scrollBehavior() {
            // 始终滚动到顶部
            return { top: 0 }
        },
    });
}
