import { createSSRApp, createApp as _createApp} from 'vue'
import App from './App.vue'
import { createRouter } from './router'
import createMeta from './plugin/meta'
import createAxios from './plugin/axios'

export function createApp(ssr: boolean) {
    const app = ssr ? createSSRApp(App) : _createApp(App);
    const router = createRouter()
    const myMeta = createMeta()
    const myAxios = createAxios()

    app.use(router)
    app.use(myMeta, {mixin: false})
    app.use(myAxios)

    return app
}
