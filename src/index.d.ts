import {AxiosPromise, AxiosRequestConfig} from "axios";

declare global {
    type commonObject<T> = {
        [key: string]: T
    }

}

declare module 'vue' {
    interface ComponentCustomProperties {
        $axios: (param: AxiosRequestConfig) => AxiosPromise;
        [key: string]: any;
    }
}

export {}
